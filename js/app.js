new Vue({
	el: '#tasks',

	data: {
		tasks:[],
		newTask: ''
	},

	methods: {
		addTask: function(){
			event.preventDefault();

			if (!this.newTask)return;

			this.tasks.push({
				body: this.newTask,
				completed: false
			});

			this.newTask = '';
		},

		removeTask: function(task){
			
			this.tasks.$remove(task);
		},

		editTask: function(task){
			this.removeTask(task);
			this.newTask = task.body;
			this.$$.newTask.focus();
		},

		toggleCompletionState: function(task){
			task.completed = !task.completed;
		},

		completeAll: function(){
			this.tasks.forEach(function(task){
				task.completed = true;
			});
		},

		archiveCompleted: function(){
			this.tasks = this.tasks.filter(function(task){
				return !task.completed;
			});
		}
	},

	computed: {
		completions: function(){
			return this.tasks.filter(function(task){
				return task.completed;
			});
		},
		remaining: function(){
			return this.tasks.filter(function(task){
				return !task.completed;
			});
		}
	},

	filters: {
		inProgress: function(tasks){
			return tasks.filter(function(task){
				return !task.completed;
			});
		},

		completed: function(tasks){
			return tasks.filter(function(task){
				return task.completed;
			});		
		}
	}
})